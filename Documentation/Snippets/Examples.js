﻿// #region Language
var userLang = navigator.language || navigator.userLanguage || 'en';
$("#example").DataTable( {
    "language": {
        "url": '/api/Language/' + userLang
    }
});
// #endregion