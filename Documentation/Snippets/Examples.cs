﻿using System;
using DataTables;

#region Language
public class LanguageController : ApiController
{
    // Default language settings, but Bootstrap Glyphicons in pagination
    private static Language eng = new Language()
    {
        Paginate = new Language.Pagination()
        {
            First = "<span class=\"glyphicon glyphicon-fast-backward\" aria-hidden=\"true\"></span>",
            Previous = "<span class=\"glyphicon glyphicon-step-backward\" aria-hidden=\"true\"></span>",
            Next = "<span class=\"glyphicon glyphicon-step-forward\" aria-hidden=\"true\"></span>",
            Last = "<span class=\"glyphicon glyphicon-fast-forward\" aria-hidden=\"true\"></span>"
        }
    };
    
    // Russian translation
    private static Language rus = new Language()
    {
        Aria = new Language.ARIA
        {
            Paginate = new Language.Pagination
            {
                First = "На первую страницу",
                Previous = "На предыдущую страницу",
                Next = "На следующую страницу",
                Last = "На последнюю страницу"
            },
            SortAscending = " - сортировать по возрастанию",
            SortDescending = " - сортировать по убыванию"
        },
        DecimalDelimiter = ",",
        EmptyTable = "Нет данных для отображения.",
        Info = "Отображается страница _PAGE_ из _PAGES_.",
        InfoEmpty = "Нет данных для отображения.",
        InfoFiltered = "Фильтру удовлетворяют _TOTAL_ записей из _MAX_.",
        LengthMenu = "Отображать _MENU_ строк на странице",
        LoadingRecords = "Загрузка данных в таблицу ...",
        Paginate = new Language.Pagination()
        {
            First = "<span class=\"glyphicon glyphicon-fast-backward\" aria-hidden=\"true\"></span>",
            Previous = "<span class=\"glyphicon glyphicon-step-backward\" aria-hidden=\"true\"></span>",
            Next = "<span class=\"glyphicon glyphicon-step-forward\" aria-hidden=\"true\"></span>",
            Last = "<span class=\"glyphicon glyphicon-fast-forward\" aria-hidden=\"true\"></span>"
        },
        Processing = "Обработка запроса ...",
        Search = "Найти:",
        SearchPlaceholder = "Искомая строка",
        Thousands = " ",
        ZeroRecords = "Нет данных, удовлетворяющих условиям запроса"
    };
    
    public Language Get(string id)
    {
        var comparator = StringComparison.CurrentCultureIgnoreCase;
        if (id.Equals("ru", comparator) || id.StartsWith("ru-", comparator))
            return rus;
        else
            return eng;
    }
}
#endregion

#region Request Conversion
public string BuildQuery(string tableName, Request request)
{
    var globalSearch = request.Columns
        .Where(col => col.Searchable)
        .Select(col => string.Format("{0} LIKE %{1}%", col.Data, request.Search.Value));
    var globalCondition = string.Join(" OR ", globalSearch);
    var localSearch = request.Columns
        .Where(col => col.Searchable && col.Search != null && !string.IsNullOrEmpty(col.Search.Value))
        .Select(col => string.Format("{0} LIKE %{1}%", col.Data, col.Search.Value));
    var localCondition = string.Join(" AND ", localSearch);
    var condition = string.Empty;
    if (!string.IsNullOrEmpty(localCondition))
        condition += localCondition;
    if (!string.IsNullOrEmpty(localCondition) && !string.IsNullOrEmpty(globalCondition))
        condition += " AND ";
    if (!string.IsNullOrEmpty(globalCondition))
        condition += globalCondition;
    var orderSettings = (request.Order ?? System.Linq.Enumerable.Empty<OrderParameters>())
        .Select(op => new { field = request.Columns[op.Column].Data, orderable = request.Columns[op.Column].Orderable, direction = op.Dir })
        .Where(col => col.orderable)
        .Select(col => string.Format("{0} {1}", col.field, col.direction.ToString().ToUpper()));
    var order = string.Join(", ", orderSettings);
    var query = "SELECT * FROM " + tableName +
        (string.IsNullOrEmpty(condition) ? string.Empty : " WHERE " + condition) +
        " ORDER BY " + (string.IsNullOrEmpty(order) ? "1" : order) +
        string.Format(" OFFSET {0} ROWS FETCH NEXT {1} ROWS ONLY", request.Start, request.Length);
    return query;
}
#endregion

#region Create Response
public class MyController : ApiController
{
    public DataTables.Response<MyClass> Post([FromBody]Request request)
    {
        var response = new Response<MyClass>() 
        { 
            Draw = request.Draw 
        };
        MyClass[] rows;
        int total, filtered;
        try
        {
            // Do something to get records from data source in accordance with request conditions, 
            // calculate total number of records before and after filtering has been applied.

            response.Data = rows;
            response.Total = total;
            response.Filtered = filtered;
        }
        catch (Exception ex)
        {
            response.Error = ex.Message;
        }
        return response;
    }
}
#endregion