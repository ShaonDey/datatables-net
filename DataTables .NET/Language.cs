﻿using Newtonsoft.Json;

namespace DataTables
{
    /// <summary xml:lang="en">
    /// Language configuration options.
    /// </summary>
    /// <remarks>
    /// Use instanses of this class to modifiy strings that DataTables uses in its user interface.
    /// </remarks>
    /// <seealso xml:lang="en" href="https://datatables.net/reference/option/language" target="_blank">
    /// «language» option in DataTables plug-in documentation
    /// </seealso>
    /// <example xml:lang="en">
    /// It creates Web API controller that returns language configuration options for DataTables 
    /// in accordance with provided RFC 5646 language tag. The controller returns either russian 
    /// or english translation of DataTables strings in dependence on provided <c>id</c> value.
    /// <code language="CSharp" source="Snippets\Examples.cs" region="Language" />
    /// Then you could detect language on the client-side and use 
    /// <see href="https://datatables.net/reference/option/language.url">language.url</see> 
    /// option to load suitable settings:
    /// <code language="JavaScript" source="Snippets\Examples.js" region="Language" />
    /// </example>
    public class Language
    {
        /// <summary xml:lang="en">
        /// Language strings used for WAI-ARIA labels.
        /// </summary>
        /// <inheritdoc cref="DataTables.Language.ARIA"/>
        [JsonProperty(PropertyName = "aria", NullValueHandling = NullValueHandling.Ignore)]
        public ARIA Aria { get; set; }

        /// <summary xml:lang="en">
        /// Decimal place character.
        /// </summary>
        /// <value xml:lang="en">Any character can be set as the decimal place.</value>
        /// <remarks xml:lang="en">
        /// <para>This will be used to correctly adjust DataTables' type detection and sorting algorithms to sort numbers in your table. 
        /// This option is a little unusual as DataTables will never display a formatted, floating point number 
        /// so this option only effects how it parses the read data.</para>
        /// <para>If the value is an <see cref="System.String.Empty">empty string</see> or <see langword="null"/> 
        /// a dot is assumed to be the character used for the decimal place.</para>
        /// </remarks>
        /// <example xml:lang="en">
        /// <para>The following code sets thousands separator to be a quote mark and allows to detect 
        /// and sort numbers which use a comma as a decimal place.</para>
        /// <code language="CSharp"><![CDATA[
        /// var langOptions = new Language();
        /// langOptions.DecimalDelimiter = ",";
        /// langOptions.Thousands = " ";
        /// ]]></code>
        /// </example>
        /// <seealso xml:lang="en" href="https://datatables.net/reference/option/language.decimal" target="_blank">
        /// «language.decimal» option in DataTables plug-in documentation
        /// </seealso>
        [JsonProperty(PropertyName = "decimal", NullValueHandling = NullValueHandling.Ignore)]
        public string DecimalDelimiter { get; set; }

        /// <summary xml:lang="en">
        /// Table has no records string.
        /// </summary>
        /// <remarks xml:lang="en">
        /// This string is shown when the table is empty of data (regardless of filtering) - i.e. there are zero records in the data source. 
        /// If it is not given, the value of <see cref="DataTables.Language.ZeroRecords"/> will be used instead (either the default or given value).
        /// </remarks>
        /// <example xml:lang="en">
        /// <code language="CSharp"><![CDATA[
        /// var langOptions = new Language();
        /// langOptions.EmptyTable = "No data available in the table";
        /// ]]></code>
        /// </example>
        /// <seealso xml:lang="en" href="https://datatables.net/reference/option/language.emptyTable" target="_blank">
        /// «language.emptyTable» option in DataTables plug-in documentation
        /// </seealso>
        [JsonProperty(PropertyName = "emptyTable", NullValueHandling = NullValueHandling.Ignore)]
        public string EmptyTable { get; set; }

        /// <summary xml:lang="en">
        /// Table summary information display string.
        /// </summary>
        /// <remarks xml:lang="en">
        /// <para>Provides information about the records that is current on display on the page.</para>
        /// <para subject="tokens">These tokens can be placed anywhere in the string, or removed as needed by the language requires:</para>
        /// <list type="table" subject="tokens">
        /// <listheader><term>Placeholder</term><description>Content</description></listheader>
        /// <item><term><c>_START_</c></term><description>Display index of the first record on the current page</description></item>
        /// <item><term><c>_END_</c></term><description>Display index of the last record on the current page</description></item>
        /// <item><term><c>_TOTAL_</c></term><description>Number of records in the table after filtering</description></item>
        /// <item><term><c>_MAX_</c></term><description>Number of records in the table without filtering</description></item>
        /// <item><term><c>_PAGE_</c></term><description>Current page number</description></item>
        /// <item><term><c>_PAGES_</c></term><description>Total number of pages of data in the table</description></item>
        /// </list>
        /// </remarks>
        /// <example xml:lang="en">
        /// <code language="CSharp"><![CDATA[
        /// var langOptions = new Language();
        /// langOptions.Info = "Showing _START_ to _END_ of _TOTAL_ entries";
        /// ]]></code>
        /// </example>
        /// <seealso xml:lang="en" href="https://datatables.net/reference/option/language.info" target="_blank">
        /// «language.info» option in DataTables plug-in documentation
        /// </seealso>
        [JsonProperty(PropertyName = "info", NullValueHandling = NullValueHandling.Ignore)]
        public string Info { get; set; }

        /// <summary xml:lang="en">
        /// Table summary information string used when the table is empty of records.
        /// </summary>
        /// <remarks xml:lang="en">Display information string for when the table is empty.</remarks>
        /// <example xml:lang="en">
        /// <code language="CSharp"><![CDATA[
        /// var langOptions = new Language();
        /// langOptions.InfoEmpty = "No entries to show";
        /// ]]></code>
        /// </example>
        /// <seealso xml:lang="en" href="https://datatables.net/reference/option/language.infoEmpty" target="_blank">
        /// «language.infoEmpty» option in DataTables plug-in documentation
        /// </seealso>
        [JsonProperty(PropertyName = "infoEmpty", NullValueHandling = NullValueHandling.Ignore)]
        public string InfoEmpty { get; set; }

        /// <summary xml:lang="en">
        /// Appended string to the <see cref="DataTables.Language.Info">summary information</see> when the table is filtered.
        /// </summary>
        /// <remarks xml:lang="en">
        /// <para>Provides information of how strong the filtering is.</para>
        /// <inheritdoc cref="Language.Info" select="/remarks/*[@subject='tokens']"/>
        /// </remarks>
        /// <example xml:lang="en">
        /// <code language="CSharp"><![CDATA[
        /// var langOptions = new Language();
        /// langOptions.InfoFiltered = " - filtered from _MAX_ records";
        /// ]]></code>
        /// </example>
        /// <seealso xml:lang="en" href="https://datatables.net/reference/option/language.infoFiltered" target="_blank">
        /// «language.infoFiltered» option in DataTables plug-in documentation
        /// </seealso>
        [JsonProperty(PropertyName = "infoFiltered", NullValueHandling = NullValueHandling.Ignore)]
        public string InfoFiltered { get; set; }

        /// <summary xml:lang="en">
        /// Page length options string.
        /// </summary>
        /// <remarks xml:lang="en">
        /// Allows to specify the entries in the length drop down menu that DataTables shows when pagination is enabled. 
        /// Use <c>_MENU_</c> placeceholder to generate a default select list of 10, 25, 50 and 100 or any other value specified by 
        /// <see href="https://datatables.net/reference/option/lengthMenu">lengthMenu</see> DataTables initialisation option.
        /// </remarks>
        /// <example xml:lang="en">
        /// <para>Language change only:</para>
        /// <code language="CSharp"><![CDATA[
        /// var langOptions = new Language();
        /// langOptions.LengthMenu = "Display _MENU_ records";
        /// ]]></code>
        /// <para>Language and options change:</para>
        /// <code language="CSharp"><![CDATA[
        /// var langOptions = new Language();
        /// langOptions.LengthMenu = "Display <select class=\"form-control input-sm\">" +
        ///     "<option value=\"10\">10</option>" +
        ///     "<option value=\"20\">20</option>" +
        ///     "<option value=\"30\">30</option>" +
        ///     "<option value=\"40\">40</option>" +
        ///     "<option value=\"50\">50</option>" +
        ///     "<option value=\"-1\">All</option>" +
        ///     "</select> records";
        /// ]]></code>
        /// </example>
        /// <seealso xml:lang="en" href="https://datatables.net/reference/option/language.lengthMenu" target="_blank">
        /// «language.lengthMenu» option in DataTables plug-in documentation
        /// </seealso>
        [JsonProperty(PropertyName = "lengthMenu", NullValueHandling = NullValueHandling.Ignore)]
        public string LengthMenu { get; set; }

        /// <summary xml:lang="en">
        /// Loading information display string - shown when AJAX loading data.
        /// </summary>
        /// <remarks>
        /// This message is shown in an empty row in the table to indicate to the end user the the data is being loaded.
        /// </remarks>
        /// <example xml:lang="en">
        /// <code language="CSharp"><![CDATA[
        /// var langOptions = new Language();
        /// langOptions.LoadingRecords = "Loading. Please wait.";
        /// ]]></code>
        /// </example>
        /// <seealso xml:lang="en" href="https://datatables.net/reference/option/language.loadingRecords" target="_blank">
        /// «language.loadingRecords» option in DataTables plug-in documentation
        /// </seealso>
        [JsonProperty(PropertyName = "loadingRecords", NullValueHandling = NullValueHandling.Ignore)]
        public string LoadingRecords { get; set; }

        /// <summary xml:lang="en">
        /// Pagination specific language strings.
        /// </summary>
        /// <example>
        /// The following code applies icons from Glyphicon Halflings set for pagination controls. 
        /// It expects the Bootstrap is used on your page.
        /// <code lang="CSharp"><![CDATA[
        /// var langOptions = new Language();
        /// langOptions.Paginate = new Language.Pagination()
        /// {
        ///     First = "<span class=\"glyphicon glyphicon-fast-backward\" aria-hidden=\"true\"></span>",
        ///     Previous = "<span class=\"glyphicon glyphicon-step-backward\" aria-hidden=\"true\"></span>",
        ///     Next = "<span class=\"glyphicon glyphicon-step-forward\" aria-hidden=\"true\"></span>",
        ///     Last = "<span class=\"glyphicon glyphicon-fast-forward\" aria-hidden=\"true\"></span>"
        /// }
        /// ]]></code>
        /// </example>
        /// <seealso xml:lang="en" href="https://datatables.net/reference/option/language.paginate" target="_blank">
        /// «language.paginate» option in DataTables plug-in documentation
        /// </seealso>
        [JsonProperty(PropertyName = "paginate", NullValueHandling = NullValueHandling.Ignore)]
        public Pagination Paginate { get; set; }

        /// <summary xml:lang="en">
        /// Processing indicator string.
        /// </summary>
        /// <remarks>The message is displayed when the table is processing a sort command or similar user action.</remarks>
        /// <example xml:lang="en">
        /// <code language="CSharp"><![CDATA[
        /// var langOptions = new Language();
        /// langOptions.Processing = "The table is processing a request...";
        /// ]]></code>
        /// </example>
        /// <seealso xml:lang="en" href="https://datatables.net/reference/option/language.processing" target="_blank">
        /// «language.processing» option in DataTables plug-in documentation
        /// </seealso>
        [JsonProperty(PropertyName = "processing", NullValueHandling = NullValueHandling.Ignore)]
        public string Processing { get; set; }

        /// <summary xml:lang="en">
        /// Search input string.
        /// </summary>
        /// <remarks xml:lang="en">
        /// String for the label of filtering input control. 
        /// The token <c>_INPUT_</c>, if used in the string, is replaced with the HTML text box for the filtering input 
        /// allowing control over where it appears in the string. 
        /// If <c>_INPUT_</c> is not given then the input box is appended to the string automatically.
        /// </remarks>
        /// <example xml:lang="en">
        /// <para>If the <c>_INPUT_</c> placeholder is not given, the input text box will be appended at the end automatically.</para>
        /// <code language="CSharp"><![CDATA[
        /// var langOptions = new Language();
        /// langOptions.Search = "Search:";
        /// ]]></code>
        /// <para>Use <c>_INPUT_</c> placeholder to specify where the filter should appear.</para>
        /// <code language="CSharp"><![CDATA[
        /// var langOptions = new Language();
        /// langOptions.Search = "Apply filter _INPUT_ to table";
        /// ]]></code>
        /// </example>
        /// <seealso xml:lang="en" href="https://datatables.net/reference/option/language.search" target="_blank">
        /// «language.search» option in DataTables plug-in documentation
        /// </seealso>
        [JsonProperty(PropertyName = "search", NullValueHandling = NullValueHandling.Ignore)]
        public string Search { get; set; }

        /// <summary xml:lang="en">
        /// Search input element placeholder attribute.
        /// </summary>
        /// <remarks>Sets placeholder attribute value in a DataTable's search input.</remarks>
        /// <example xml:lang="en">
        /// <code language="CSharp"><![CDATA[
        /// var langOptions = new Language();
        /// langOptions.Search = "Apply filter _INPUT_ to table";
        /// langOptions.SearchPlaceholder = "Type term here";
        /// ]]></code>
        /// </example>
        /// <seealso xml:lang="en" href="https://datatables.net/reference/option/language.searchPlaceholder" target="_blank">
        /// «language.searchPlaceholder» option in DataTables plug-in documentation
        /// </seealso>
        [JsonProperty(PropertyName = "searchPlaceholder", NullValueHandling = NullValueHandling.Ignore)]
        public string SearchPlaceholder { get; set; }

        /// <summary xml:lang="en">
        /// Thousands separator.
        /// </summary>
        /// <remarks xml:lang="en">
        /// Unlike the <see cref="DataTables.Language.DecimalDelimiter"/> property, 
        /// the thousands separator option is used for output of information only  
        /// and does not effect how DataTables reads numeric data.
        /// </remarks>
        /// <inheritdoc cref="DataTables.Language.DecimalDelimiter" select="example"/>
        /// <seealso xml:lang="en" href="https://datatables.net/reference/option/language.thousands" target="_blank">
        /// «language.thousands» option in DataTables plug-in documentation
        /// </seealso>
        [JsonProperty(PropertyName = "thousands", NullValueHandling = NullValueHandling.Ignore)]
        public string Thousands { get; set; }

        /// <summary xml:lang="en">
        /// Table empty as a result of filtering string.
        /// </summary>
        /// <remarks xml:lang="en">
        /// Text shown inside the table records when the is no information to be displayed after filtering. 
        /// If there is no records in source then message from <see cref="DataTables.Language.EmptyTable"/> property is displayed.
        /// </remarks>
        /// <example xml:lang="en">
        /// <code language="CSharp"><![CDATA[
        /// var langOptions = new Language();
        /// langOptions.ZeroRecords = "No records to display";
        /// ]]></code>
        /// </example>
        /// <seealso xml:lang="en" href="https://datatables.net/reference/option/language.zeroRecords" target="_blank">
        /// «language.zeroRecords» option in DataTables plug-in documentation
        /// </seealso>
        [JsonProperty(PropertyName = "zeroRecords", NullValueHandling = NullValueHandling.Ignore)]
        public string ZeroRecords { get; set; }

        /// <summary xml:lang="en">
        /// Language strings used for WAI-ARIA specific attributes.
        /// </summary>
        /// <remarks>
        /// Strings, that are used for WAI-ARIA labels and controls, are not actually visible on the page, 
        /// but will be read by screen readers, and thus must be internationalised as well. 
        /// </remarks>
        /// <seealso xml:lang="en" href="https://datatables.net/reference/option/language.aria" target="_blank">
        /// «language.aria» option in DataTables plug-in documentation
        /// </seealso>
        public class ARIA
        {
            /// <summary xml:lang="en">
            /// WAI-ARIA labels for pagination buttons.
            /// </summary>
            /// <example>
            /// The following code set custom <c>aria-label</c> attribute values for the pagination buttons.
            /// <code lang="CSharp"><![CDATA[
            /// var aria = new ARIA();
            /// aria.Paginate = new Language.Pagination()
            /// {
            ///     First = "To first page",
            ///     Previous = "To previous page",
            ///     Next = "To next page",
            ///     Last = "To last page"
            /// }
            /// ]]></code>
            /// </example>
            /// <seealso xml:lang="en" href="https://datatables.net/reference/option/language.aria.paginate">
            /// «language.aria.paginate» option in DataTables plug-in documentation
            /// </seealso>
            [JsonProperty(PropertyName = "paginate", NullValueHandling = NullValueHandling.Ignore)]
            public Pagination Paginate { get; set; }

            /// <summary xml:lang="en">
            /// ARIA label that is added to the table headers when the column may be sorted ascending by activating the column.
            /// </summary>
            /// <remarks xml:lang="en">
            /// The column header text is prefixed to this string.
            /// </remarks>
            /// <example>
            /// The following code set custom suffixes for <c>aria-label</c> attributes of column sorting controls.
            /// <code lang="CSharp"><![CDATA[
            /// var aria = new ARIA()
            /// {
            ///     SortAscending = " - click/return to sort ascending",
            ///     SortDescending = " - click/return to sort descending"
            /// };
            /// ]]></code>
            /// </example>
            /// <seealso xml:lang="en" href="https://datatables.net/reference/option/language.aria.sortAscending" target="_blank">
            /// «language.aria.sortAscending» option in DataTables plug-in documentation
            /// </seealso>
            [JsonProperty(PropertyName = "sortAscending", NullValueHandling = NullValueHandling.Ignore)]
            public string SortAscending { get; set; }

            /// <summary xml:lang="en">
            /// ARIA label that is added to the table headers when the column may be sorted descending by activing the column.
            /// </summary>
            /// <remarks xml:lang="en">
            /// The column header text is prefixed to this string.
            /// </remarks>
            /// <inheritdoc cref="DataTables.Language.ARIA.SortAscending" select="example"/>
            /// <seealso xml:lang="en" href="https://datatables.net/reference/option/language.aria.sortDescending" target="_blank">
            /// «language.aria.sortDescending» option in DataTables plug-in documentation
            /// </seealso>
            [JsonProperty(PropertyName = "sortDescending", NullValueHandling = NullValueHandling.Ignore)]
            public string SortDescending { get; set; }
        }

        /// <summary xml:lang="en">
        /// Settings for pagination.
        /// </summary>
        /// <seealso cref="DataTables.Language.Paginate"/>
        /// <seealso cref="DataTables.Language.ARIA.Paginate"/>
        public class Pagination
        {
            /// <summary xml:lang="en">
            /// Text for pagination button to take the user to the first page.
            /// </summary>
            /// <seealso xml:lang="en" href="https://datatables.net/reference/option/language.paginate.first" target="_blank">
            /// «language.paginate.first» option in DataTables plug-in documentation
            /// </seealso>
            /// <seealso xml:lang="en" href="https://datatables.net/reference/option/language.aria.paginate.first" target="_blank">
            /// «language.aria.paginate.first» option in DataTables plug-in documentation
            /// </seealso>
            [JsonProperty(PropertyName = "first", NullValueHandling = NullValueHandling.Ignore)]
            public string First { get; set; }

            /// <summary xml:lang="en">
            /// Text for pagination button to take the user to the previous page.
            /// </summary>
            /// <seealso xml:lang="en" href="https://datatables.net/reference/option/language.paginate.previous" target="_blank">
            /// «language.paginate.previous» option in DataTables plug-in documentation
            /// </seealso>
            /// <seealso xml:lang="en" href="https://datatables.net/reference/option/language.aria.paginate.previous" target="_blank">
            /// «language.aria.paginate.previous» option in DataTables plug-in documentation
            /// </seealso>
            [JsonProperty(PropertyName = "previous", NullValueHandling = NullValueHandling.Ignore)]
            public string Previous { get; set; }

            /// <summary xml:lang="en">
            /// Text for pagination button to take the user to the next page.
            /// </summary>
            /// <seealso xml:lang="en" href="https://datatables.net/reference/option/language.paginate.next" target="_blank">
            /// «language.paginate.next» option in DataTables plug-in documentation
            /// </seealso>
            /// <seealso xml:lang="en" href="https://datatables.net/reference/option/language.aria.paginate.next" target="_blank">
            /// «language.aria.paginate.next» option in DataTables plug-in documentation
            /// </seealso>
            [JsonProperty(PropertyName = "next", NullValueHandling = NullValueHandling.Ignore)]
            public string Next { get; set; }

            /// <summary xml:lang="en">
            /// Text for pagination button to take the user to the last page.
            /// </summary>
            /// <seealso xml:lang="en" href="https://datatables.net/reference/option/language.paginate.last" target="_blank">
            /// «language.paginate.last» option in DataTables plug-in documentation
            /// </seealso>
            /// <seealso xml:lang="en" href="https://datatables.net/reference/option/language.aria.paginate.last" target="_blank">
            /// «language.aria.paginate.last» option in DataTables plug-in documentation
            /// </seealso>
            [JsonProperty(PropertyName = "last", NullValueHandling = NullValueHandling.Ignore)]
            public string Last { get; set; }
        }

        /// <summary xml:lang="en">
        /// Сorresponds to default language settings.
        /// </summary>
        public static Language @default = new Language();

        /// <summary>
        /// Настройки конфигурации для русского языка.
        /// </summary>
        public static Language ru = new Language()
        {
            Aria = new ARIA
            {
                Paginate = new Pagination
                {
                    First = "На первую страницу",
                    Previous = "На предыдущую страницу",
                    Next = "На следующую страницу",
                    Last = "На последнюю страницу"
                },
                SortAscending = " - сортировать по возрастанию",
                SortDescending = " - сортировать по убыванию"
            },
            DecimalDelimiter = ",",
            EmptyTable = "Нет данных для отображения.",
            Info = "Отображается страница _PAGE_ из _PAGES_.",
            InfoEmpty = "Нет данных для отображения.",
            InfoFiltered = "Фильтру удовлетворяют _TOTAL_ записей из _MAX_.",
            LengthMenu = "Отображать _MENU_ строк на странице",
            LoadingRecords = "Загрузка данных в таблицу ...",
            Paginate = new Pagination()
            {
                First = "Первая",
                Previous = "Предыдущая",
                Next = "Следующая",
                Last = "Последняя"
            },
            Processing = "Обработка запроса ...",
            Search = "Найти:",
            SearchPlaceholder = "Искомая строка",
            Thousands = " ",
            ZeroRecords = "Нет данных, удовлетворяющих условиям запроса"
        };
    }
}